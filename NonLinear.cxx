#include <iostream>
#include <vector>
#include <cassert>
#include <functional>
#include "CSSystemOfLinearEquations.hxx"

#define VARIABLE_COUNT 2
#define THRESHOLD 1E-6

typedef CS::CSMatrix VarVector;
typedef std::function<double(VarVector)> FuncType;
typedef std::vector<FuncType> FuncVector;

FuncVector makeFunctions()
{
    FuncVector result;
    result.push_back([](VarVector x) -> double {return x[1] - x[0] * x[0];});
    result.push_back([](VarVector x) -> double {return x[1] + x[0] - 2;});
    return result;
}

FuncVector makeJacobian()
{
    FuncVector result;
    result.push_back([](VarVector x) -> double {return -2 * x[0] * x[0];});
    result.push_back([](VarVector x) -> double {return 1;});
    result.push_back([](VarVector x) -> double {return 1;});
    result.push_back([](VarVector x) -> double {return 1;});
    return result;
}

CS::CSSystemOfLinearEquations makeSystem(FuncVector &functions, FuncVector &jacobian, CS::CSMatrix &x)
{
    CS::CSSystemOfLinearEquations a(VARIABLE_COUNT, false);
    
    for (int i = 0; i < jacobian.size(); i++)
    {
        a.SetNumberAt(jacobian[i](x), i % VARIABLE_COUNT, i / VARIABLE_COUNT);
    }
    
    for (int i = 0; i < VARIABLE_COUNT; i++)
    {
        a.SetNumberAt(-functions[i](x), VARIABLE_COUNT, i);
    }
    
    return a;
}

int main(int argc, char const *argv[])
{
    FuncVector functions = makeFunctions();
    FuncVector jacobian = makeJacobian();
    assert(functions.size() * VARIABLE_COUNT == jacobian.size());
    CS::CSMatrix x(1, VARIABLE_COUNT);
    for (int i = 0; i < VARIABLE_COUNT; i++)
    {
        x.SetNumberAt(0, 0, i);
    }
    
    double error = 100;
    
    while (error > THRESHOLD)
    {
        auto system = makeSystem(functions, jacobian, x);
        CS::CSMatrix result = system.SolveWithGaussMethod();
        x = x + result;
        
        CS::CSMatrix fun(1, VARIABLE_COUNT);
        for (int i = 0; i < VARIABLE_COUNT; i++)
        {
            fun[i] = functions[i](x);
        }
        error = fun.Norm();
    }
    
    x.PrintMatrix();
    
    return 0;
}