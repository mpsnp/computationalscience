#include <iostream>
#include <cassert>
#include <cmath>

#define MINIMAL_STEP_COUNT 2
#define THRESHOLD 1E-8
#define MAX_ERROR 1E6

double f(double x)
{
    return x * x * x * x;
}

double integrate(double a, double b, int stepCount)
{
    assert(stepCount >= MINIMAL_STEP_COUNT);
    assert(a <= b);
    
    double result = 0;
    
    double stepSize = (b - a) / stepCount;
    double halfStepSize = 0.5 * stepSize;
    
    for (double x = a; x < b; x += stepSize)
    {
        result += (f(x) + f(x + stepSize)) * halfStepSize;
    }
    
    return result;
}

double integrateWithAutomaticStepAdjust(double a, double b)
{
    int stepCount = MINIMAL_STEP_COUNT;
    double result = integrate(a, b, stepCount);
    double error = MAX_ERROR;
    double teta = 1.0 / 3.0;
    while (error >= THRESHOLD)
    {
        stepCount = 2 * stepCount;
        double newResult = integrate(a, b, stepCount);
        error = teta * fabs(newResult - result);
        result = newResult;
    }
    return result;
}

int main(int argc, char const *argv[])
{
    std::cout << "I = " << integrateWithAutomaticStepAdjust(0, 1) << std::endl;
    return 0;
}