#include "CSSystemOfLinearEquations.hxx"

#include <iostream>
#include <fstream>
#include <cmath>

static const double epsilon = 1E-15;
using namespace std;

CS::CSSystemOfLinearEquations::CSSystemOfLinearEquations(int variables, bool isHomohenious)
        : CSMatrix(variables + 1, variables) {
    _homohenious = isHomohenious;
}

CS::CSSystemOfLinearEquations::~CSSystemOfLinearEquations() {

}

void CS::CSSystemOfLinearEquations::ReadSystem(std::istream *stream) {
    int to = (_homohenious) ? GetHeight() : GetWidth();

    double temp;

    for (int y = 0; y < GetHeight(); ++y)
        for (int x = 0; x < to; ++x) {
            *stream >> temp;
            SetNumberAt(temp, x, y);
        }

}

void CS::CSSystemOfLinearEquations::ReadSystem() {
    ReadSystem(&cin);
}

void CS::CSSystemOfLinearEquations::ReadSystem(std::string fileName) {
    fstream inputStream;

    inputStream.open(fileName.c_str());

    ReadSystem(&inputStream);

    inputStream.close();
}

CS::CSMatrix CS::CSSystemOfLinearEquations::GetA() {
    CSMatrix A(GetHeight(), GetHeight());
    for (int i = 0; i < GetHeight(); i++)
        for (int j = 0; j < GetHeight(); j++)
            A.SetNumberAt(GetNumberAt(i, j), i, j);
    return A;
}

CS::CSMatrix CS::CSSystemOfLinearEquations::GetFree() {
    CSMatrix Free(1, GetHeight());
    for (int i = 0; i < GetHeight(); i++)
        Free.SetNumberAt(GetNumberAt(GetWidth() - 1, i), 0, i);
    return Free;
}

CS::CSMatrix CS::CSSystemOfLinearEquations::SolveWithSimpleIterationsMethod() {
    double epsilon = 0.0000001;
    CSMatrix A = GetA();
    CSMatrix B = CSMatrix(GetHeight()) - A;
    CSMatrix b = GetFree();
    CSMatrix x = CSMatrix(1, GetHeight());
    for (int i = 0; i < GetHeight(); i++)
        x.SetNumberAt(rand() % 10, 0, i);
    double nextNorm = 10000;
    while (nextNorm > epsilon && (A * x - b).Norm() > epsilon) {
        CSMatrix nextX = B * x + b;
        nextNorm = (x - nextX).Norm();
        x = nextX;
    }
    return x;
}

CS::CSMatrix CS::CSSystemOfLinearEquations::SolveWithGaussMethod() {
    for (int i = 0; i < GetHeight(); ++i) {
        if (GetNumberAt(i, i) == 0.0) {
            int rowForSwap = i + 1;
            while (rowForSwap < GetHeight() && GetNumberAt(i, rowForSwap) == 0.0)
                rowForSwap++;
            if (rowForSwap < GetHeight())
                SwapRows(i, rowForSwap);
            else
                continue;
        }
        for (int j = i + 1; j < GetHeight(); ++j)
            AppendRowToRowWithMultiplier(i, j, -GetNumberAt(i, j) / GetNumberAt(i, i));
    }

    if (GetNumberAt(GetWidth() - 1, GetHeight() - 1) == GetNumberAt(GetHeight() - 1, GetHeight() - 1) &&
        GetNumberAt(GetHeight() - 1, GetHeight() - 1) == 0) {
        cout << "No any results" << endl;
    }
    else {
        CSMatrix result = CSMatrix(1, GetHeight());
        for (int i = GetHeight() - 1; i >= 0; --i) {
            double sum = 0;
            for (int j = i + 1; j < GetHeight(); ++j)
                sum += GetNumberAt(j, i) * result.GetNumberAt(0, j);
            result.SetNumberAt((GetNumberAt(GetWidth() - 1, i) - sum) / GetNumberAt(i, i), 0, i);
        }
        return result;
    }
    return CSMatrix(0, 0);
}

int CS::CSSystemOfLinearEquations::FindMainElement(int startingRow) {
    int maxRow = startingRow;
    double max = GetNumberAt(startingRow, maxRow);
    for (int i = startingRow + 1; i < GetHeight(); i++) {
        double d = GetNumberAt(startingRow, i);
        if (fabs(d) > max) {
            max = d;
            maxRow = i;
        }
    }
    return maxRow;
}

CS::CSMatrix CS::CSSystemOfLinearEquations::SolveWithGaussDiagonalMethod() {
    for (int i = 0; i < GetHeight(); i++) {
        int mainElementRow = FindMainElement(i);
        if (GetNumberAt(i, mainElementRow) > epsilon)
            SwapRows(i, mainElementRow);
        else {
            cout << "Matrix is singular, sorry :)" << endl;
            exit(1);
        }

        MultiplyRowBy(i, 1.0 / GetNumberAt(i, i));

        for (int j = 0; j < GetHeight(); j++) {
            if (i != j) {
                AppendRowToRowWithMultiplier(i, j, -1 * GetNumberAt(i, j));
            }
        }
    }

    CSMatrix result = CSMatrix(1, GetHeight());
    for (int i = 0; i < GetHeight(); i++) {
        result.SetNumberAt(GetNumberAt(GetWidth() - 1, i), 0, i);
    }
    return result;
}