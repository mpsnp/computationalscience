#ifndef SYSTEM_OF_LINEAR_EQUATIONS_H
#define SYSTEM_OF_LINEAR_EQUATIONS_H

#include <string>

#include "CSMatrix.hxx"

namespace CS
{
	class CSSystemOfLinearEquations : public CSMatrix
	{
	private:
		bool _homohenious;
	public:
		CSSystemOfLinearEquations(int variables, bool isHomohenious);
		~CSSystemOfLinearEquations();

		void ReadSystem();
		void ReadSystem(std::istream *stream);
		void ReadSystem(std::string fileName);
		CSMatrix GetA();
		CSMatrix GetFree();
		CSMatrix SolveWithGaussMethod();
		CSMatrix SolveWithSimpleIterationsMethod();
		CSMatrix SolveWithGaussDiagonalMethod();

		int FindMainElement(int startingRow);
	};
};

#endif