#include "CSSystemOfLinearEquations.hxx"
#include "CSMatrix.hxx"
#include <iostream>
#include <cassert>
#include <cmath>
#include <cstring>

using namespace std;

CS::CSMatrix::CSMatrix(int size)
        : CSMatrix(size, size) {
    for (int i = 0; i < size; i++)
        _matrix[i][i] = 1;
}

CS::CSMatrix::CSMatrix(int width, int height) {
    _width = width;
    _height = height;
    _matrix = new double *[_width];
    for (int x = 0; x < width; ++x) {
        _matrix[x] = new double[_height];
        memset(_matrix[x], 0, sizeof(double) * _height);
    }
}

CS::CSMatrix::CSMatrix(CS::CSMatrix const &matrix) {
    _width = matrix._width;
    _height = matrix._height;
    _matrix = new double *[_width];
    for (int x = 0; x < _width; ++x) {
        _matrix[x] = new double[_height];
        for (int j = 0; j < _height; ++j) {
            _matrix[x][j] = matrix._matrix[x][j];
        }
    }
}

void CS::CSMatrix::operator=(const CS::CSMatrix &right) {
    CSMatrix tmp(right);
    std::swap(_matrix, tmp._matrix);
    std::swap(_width, tmp._width);
    std::swap(_height, tmp._height);
}

double &CS::CSMatrix::operator[](const int position) {
    return _matrix[position % _width][position / _width];
}

CS::CSMatrix::~CSMatrix() {
    for (int i = 0; i < _width; ++i)
        delete[]_matrix[i];
    delete[]_matrix;
}

void CS::CSMatrix::PrintMatrix() const {
    for (int y = 0; y < _height; ++y) {
        for (int x = 0; x < _width; ++x)
            cout << _matrix[x][y] << ' ';
        cout << endl;
    }
}

double CS::CSMatrix::Norm() {
    assert(GetWidth() == 1);
    double sum = 0;
    for (int i = 0; i < GetHeight(); i++) {
        sum += GetNumberAt(0, i) * GetNumberAt(0, i);
    }
    return sqrt(sum);
}

void CS::CSMatrix::ReadMatrix() {
    for (int y = 0; y < _height; ++y)
        for (int x = 0; x < _width; ++x)
            cin >> _matrix[x][y];
}

void CS::CSMatrix::SetNumberAt(double number, int x, int y) {
    assert(x < _width && y < _height && x >= 0 && y >= 0);
    _matrix[x][y] = number;
}

double CS::CSMatrix::GetNumberAt(int x, int y) const {
    assert(x < _width && y < _height && x >= 0 && y >= 0);
    return _matrix[x][y];
}

int CS::CSMatrix::GetWidth() const {
    return _width;
}

int CS::CSMatrix::GetHeight() const {
    return _height;
}

void CS::CSMatrix::Transpose() {
    for (int x = 0; x < _width; ++x)
        for (int y = 0; y < x; ++y)
            swap(_matrix[x][y], _matrix[y][x]);
}

void CS::CSMatrix::SwapRows(int row1, int row2) {
    assert(row1 < _height && row2 < _height);
    for (int i = 0; i < _width; ++i)
        swap(_matrix[i][row1], _matrix[i][row2]);
}

void CS::CSMatrix::SwapColumns(int col1, int col2) {
    assert(col1 < _width && col2 < _width);
    swap(_matrix[col1], _matrix[col2]);
}

void CS::CSMatrix::AppendRowToRowWithMultiplier(int row1, int row2, double multiplier) {
    assert(row1 < _height && row2 < _height);
    for (int x = 0; x < _width; ++x)
        _matrix[x][row2] += multiplier * _matrix[x][row1];
}

CS::CSMatrix CS::operator+(const CS::CSMatrix &left, const CS::CSMatrix &right) {
    assert(left.GetWidth() == right.GetWidth() && left.GetHeight() == right.GetHeight());
    CSMatrix result = CSMatrix(left.GetWidth(), left.GetHeight());

    for (int x = 0; x < left.GetWidth(); ++x)
        for (int y = 0; y < left.GetHeight(); ++y)
            result.SetNumberAt(left.GetNumberAt(x, y) + right.GetNumberAt(x, y), x, y);

    return result;
}

CS::CSMatrix CS::operator-(const CS::CSMatrix &left, const CS::CSMatrix &right) {
    assert(left.GetWidth() == right.GetWidth() && left.GetHeight() == right.GetHeight());
    CSMatrix result = CSMatrix(left.GetWidth(), left.GetHeight());

    for (int x = 0; x < left.GetWidth(); ++x)
        for (int y = 0; y < left.GetHeight(); ++y)
            result.SetNumberAt(left.GetNumberAt(x, y) - right.GetNumberAt(x, y), x, y);

    return result;
}

CS::CSMatrix CS::operator*(const CS::CSMatrix &left, const CS::CSMatrix &right) {
    assert(left.GetWidth() == right.GetHeight());
    CSMatrix result = CSMatrix(right.GetWidth(), left.GetHeight());

    for (int x = 0; x < result.GetWidth(); ++x)
        for (int y = 0; y < result.GetHeight(); ++y) {
            double sum = 0;
            for (int i = 0; i < left.GetWidth(); ++i)
                sum += left.GetNumberAt(i, y) * right.GetNumberAt(x, i);
            result.SetNumberAt(sum, x, y);
        }

    return result;
}

void CS::CSMatrix::MultiplyRowBy(int row, double multiplier) {
    for (int i = 0; i < GetWidth(); i++) {
        _matrix[i][row] = multiplier * _matrix[i][row];
    }
}