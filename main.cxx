#include "CSMatrix.hxx"
#include "CSSystemOfLinearEquations.hxx"

#include <iostream>

using namespace CS;

int main(int argc, char const *argv[])
{
	int varCount = 5;
	// std::cin >> varCount;
	CSSystemOfLinearEquations sole = CSSystemOfLinearEquations(varCount, false);
    sole.ReadSystem("input.txt");
//    sole.SolveWithSimpleIterationsMethod();
//    sole.SolveWithGaussMethod();
    const CSMatrix &matrix = sole.SolveWithGaussDiagonalMethod();
    sole.PrintMatrix();
    matrix.PrintMatrix();
	return 0;
}