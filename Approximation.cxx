#include "CSMatrix.hxx"
#include "CSSystemOfLinearEquations.hxx"

#include <iostream>
#include <cmath>
#include <fstream>

using namespace CS;
using namespace std;

CSMatrix approximate(int varCount)
{
    fstream inputStream;
    inputStream.open("approximation.txt");
    
    int numCount;
    inputStream >> numCount;
    
    CSMatrix input(numCount, 2);
    
    for (int i = 0; i < numCount; i++)
    {
        double x,y;
        inputStream >> x >> y;
        input.SetNumberAt(x, i, 0);
        input.SetNumberAt(y, i, 1);
    }
    
    CSSystemOfLinearEquations sole = CSSystemOfLinearEquations(varCount, false);
    for (int i = 0; i < varCount; i++)
    {
        for (int j = 0; j < varCount; j++)
        {
            double sum = 0;
            for (int k = 0; k < numCount; k++)
                sum += pow(input.GetNumberAt(k, 0), i + j);
            sole.SetNumberAt(sum, i, j);
        }
    }
    
    for (int i = 0; i < varCount; i++)
    {
        double sum = 0;
        for (int j = 0; j < numCount; j++)
            sum += pow(input.GetNumberAt(j, 0), i) * input.GetNumberAt(j, 1);
        sole.SetNumberAt(sum, varCount, i);
    }
    
    return sole.SolveWithGaussMethod();
}

int main(int argc, char const *argv[])
{
    auto result = approximate(3);
    for (int i = 0; i < result.GetHeight(); i++)
    {
        cout << "a[" << i << "] = " << result.GetNumberAt(0, i) << endl;
    }
	return 0;
}