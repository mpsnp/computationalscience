#ifndef MATRIX_H
#define MATRIX_H

namespace CS {
    class CSMatrix {
    private:
        double **_matrix;
        int _width;
        int _height;
    public:
        CSMatrix(int size);

        CSMatrix(int width, int height);

        CSMatrix(CSMatrix const &matrix);

        ~CSMatrix();

        int GetWidth() const;

        int GetHeight() const;

        void ReadMatrix();

        void PrintMatrix() const;

        void SetNumberAt(double number, int x, int y);

        double GetNumberAt(int x, int y) const;

        void Transpose();

        void SwapRows(int row1, int row2);

        void SwapColumns(int col1, int col2);

        void MultiplyRowBy(int row, double multiplier);

        void AppendRowToRowWithMultiplier(int row1, int row2, double multiplier);

        double Norm();

        void operator=(const CSMatrix &right);

        double &operator[](const int pos);
    };

    CSMatrix operator+(const CSMatrix &left, const CSMatrix &right);

    CSMatrix operator-(const CSMatrix &left, const CSMatrix &right);

    CSMatrix operator*(const CSMatrix &left, const CSMatrix &right);
};

#endif